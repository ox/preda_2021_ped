# PREDA_2021_PED

En septiembre de 2021, estoy preparando para cursar PREDA en la UNED en II revisando las PEDS. Para preparar el entorno, hasta que nos den los enunciados de las 2 PEDS, elegí Visual Code para correr la aplicación Java.

Aunque aún no he cursado las asignaturas del 1 año y 2ndo semestre, POO y EDA, poseo algún conocimiento de programación en Java. Sin embargo, el objetivo de este repositorio es básicamente familiarizarme con ese lenguaje. El contenido del mismo no debe considerarse corregido y debe someterse a criterio. No copia el código, puede ser incorrecto. 

Criterios de valoración:

![](https://gitlab.com/ox/preda_2021_ped/-/raw/sudoku/PAUTAS.png)

En la **rama PED** está la versión inicial en la que me ocupaba de:
https://gitlab.com/ox/preda_2021_ped/-/tree/ped

- Crear un proyecto Java
- Crear una clase inicial de arranque: com.preda_2021_ped.App
- Crear un entorno de tests
- Crear una clase "Tracer" para facilitar la gestión de trazas.
- Cargar un fichero json de la línea de comandos y almacenar en memoria volátil su contenido.
- Cargar un fichero plano de la línea de comandos y almacenar en memoria volátil su contenido.

[código](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/PedSolver.java)
```ts
package com.preda_ped_2021.solvers;

import com.preda_ped_2021.common.Tracer;
import com.preda_ped_2021.data.ArgsParser;

public class PedSolver extends Solver {

    private static final String PED_TASK_START = "Arrancando Solucionador Base 01";
    private static final String PED_TASK_END = "Detenido Solucionador Base 01";

    public void ejecutarTarea(String[] args) {
        Tracer.traceTaskStart(true, PED_TASK_START);

        if (ArgsParser.leerParametros(args, "plain")) {
            ArgsParser.traceParams();
        };

        if (ArgsParser.leerParametros(args, "json")) {
            ArgsParser.traceParams();
        };

        Tracer.traceTaskStart(false, PED_TASK_END);
    }
}
```

Una vez familiariado con el entorno, busqué en las prácticas de años anteriores uno de los problemas para resolver.

El elegido fue de la PED 2 de 2020-21, resolución de sudoku por **vuelta atrás**.

En la **rama sudoku** se encuentra mi mejor solución. Ver imagen.
https://gitlab.com/ox/preda_2021_ped/-/tree/sudoku. 

- Carga del fichero plano con el sudoku inicializado. Una linea del tablero coincide con una linea del fichero. Vacios con '-1'. Un blanco(espacios) como marcador entre celdas.
- Matriz[][] para almacenar los datos del tablero incial.
- Carga del fichero plano con el sudoku solución. Una linea del tablero coincide con una linea del fichero. No puede contener vacios con '-1'. Un blanco(espacios) como marcador entre celdas.
- Matriz[][] para almacenar los datos del tablero solucionado.
- [codigo](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/SudokuSolver.java#L98) Función recursiva con llamada principal en (y = 0, x = 0), pasándole el sudoku inicializado y un sudoku clonado a partir de este a modo de candidato a solución. Pretende implementar el algoritmo de **vuelta atrás**:
   - **Bucle de canditatos**, [código](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/SudokuSolver.java#L143).
   - **Función de obtención de candidatos**, [código](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/SudokuBoard.java#L208). Nota: a toro pasado, la restricción 3x3 ha sido implementada en forma especialmente horrorosa, no tomar como referencia. Nota: esta función **marca el candidato en caso de ser factible**.
   - **Condición de fin o paso recursivo**, [código](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/SudokuSolver.java#L177).
   - **Restauración** del valor vacío en el final de las **vías muertas**, [código](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/SudokuSolver.java#L225)
- El programa no se detendrá al encontrar una solución (aunque, en principio, un sudoku, por definción, solo tiene una solución.), experimentalmente, se sigue explorando caminos y trazando la profundidad antes de encontrar la incoherencia y abortar la vía. Para sorpresa del estudiante, en este proceso, el sudoku 2 produce resultados múltiples.
- Para comprobar la solución:
   - Función que comprueba si un tablero está completado (no casillas vacías)
   - Función que compara el tablero candidato con el tablero solución.

[código](https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/solvers/SudokuSolver.java#L34), función principal, *ejecutarTarea*:
```ts

/**
 * Función encargada de procesar la entrada de datos, 
 * llamar al algoritmo de vuelta atrás, y tracear tanto 
 * el proceso como la solución.
 **/
public void ejecutarTarea(String[] args) {

   // Start the chrono
   Tracer.traceTaskStart(true, PED_TASK_START);

   // Get from params (file input) the sudoku solution
   solutionBoard = loadSolution(args);

   // Get from params (file input) the sudoku initial board
   if (ArgsParser.leerParametros(args, "sudoku")) {

      SudokuBoard solvedBoard = solve(ArgsParser.board);

      // Weird idea as a sudoku is considered to have exactly 1 solution.
      for (SudokuBoard solution : solvedBoard.solutions) {
         checkSolutionIntegrity(solution);
         checkSolutionCorrectness(solutionBoard, solution);
      }

   };

   Tracer.traceTaskStart(false, PED_TASK_END);
}
````

![](preda_ped_sudoku_dev.png)
  
Se han seleccionado 2 sudokus distintos como datos de validación. Uno sencillo cuya resolución ocupa la exploración de pocas vías antes de encontrar una solución en menos de un segundo (computador convencional).  

# Sudoku 1


- https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/data/input_01.sudoku. 
- https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/data/input_01.sudoku_sol.   

![](https://gitlab.com/ox/preda_2021_ped/-/raw/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/data/input_01_sudoku.png). 
  
  
![](sudoku_release_01.png). 
  
# Log abreviado de ejecución
```pre

Logs en modo LOG
Arrancando APP Fri Sep 10 21:53:10 CEST 2021
Arrancando Solucionador Base 01 Fri Sep 10 21:53:10 CEST 2021
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_01.sudoku
. . expected [plain] Found [sudoku]
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_01.sudoku
. . expected [json] Found [sudoku]
Detenido Solucionador Base 01 Corrió por -28 milliseconds.
Inicio Fri Sep 10 21:53:10 CEST 2021
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_01.sudoku_sol
. Fichero leido ./src/main/java/com/preda_ped_2021/data/input_01.sudoku_sol
. Fichero cargado ./src/main/java/com/preda_ped_2021/data/input_01.sudoku_sol
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_01.sudoku
. Fichero leido ./src/main/java/com/preda_ped_2021/data/input_01.sudoku
. Fichero cargado ./src/main/java/com/preda_ped_2021/data/input_01.sudoku
. Tablero inicial... 
 ------ ------- -------
 . . . | 2 6 . | 7 . 1 |
 6 8 . | . 7 . | . 9 . |
 1 9 . | . . 4 | 5 . . |
 ------ ------- -------
 8 2 . | 1 . . | . 4 . |
 . . 4 | 6 . 2 | 9 . . |
 . 5 . | . . 3 | . 2 8 |
 ------ ------- -------
 . . 9 | 3 . . | . 7 4 |
 . 4 . | . 5 . | . 3 6 |
 7 . 3 | . 1 8 | . . . |
Inicio [SOLVING] 1631303590861

.  |___|  Nivel: 0, Celda: (0, 0) K: 1 Tableros: i: -1 s: -1 o: 4 1631303590863
   |0%| [>                                                                                                    ]   (0,0), K: 1, Nivel: 1, Max Nivel: 1
   | X |  Nivel: 0 (0, 0) K: 1 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 1, L: 0, Total 5 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 2 Tableros: i: -1 s: -1 o: 4 1631303590871
   |0%| [>                                                                                                    ]   (0,0), K: 2, Nivel: 1, Max Nivel: 1
   | X |  Nivel: 0 (0, 0) K: 2 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 2, L: 0, Total 2 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 3 Tableros: i: -1 s: -1 o: 4 1631303590878
   |1%| [=>                                                                                                   ]   (0,4), K: 1, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 1 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 3 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 2, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 2 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 7 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 3, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 3 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 9 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 4, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 4 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 12 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 5, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 5 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 15 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 6, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 6 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 19 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 7, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 7 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 22 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 8, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 8 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 25 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 9, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 9 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 3, L: 0, Total 28 ms.

   | X |  Nivel: 0 (0, 0) K: 3 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 3, L: 0, Total 30 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 4 Tableros: i: -1 s: -1 o: 4 1631303590978
   |1%| [=>                                                                                                   ]   (0,4), K: 1, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 1 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 1 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 2, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (4, 0) K: 2 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 5 ms.
  |44%| [============================================>                                                        ]  (8,8), K: 9, Nivel: 89, Max Nivel: 89
. Solución encontrada 
 ------ ------- -------
 4 3 5 | 2 6 9 | 7 8 1 |
 6 8 2 | 5 7 1 | 4 9 3 |
 1 9 7 | 8 3 4 | 5 6 2 |
 ------ ------- -------
 8 2 6 | 1 9 5 | 3 4 7 |
 3 7 4 | 6 8 2 | 9 1 5 |
 9 5 1 | 7 4 3 | 6 2 8 |
 ------ ------- -------
 5 1 9 | 3 2 6 | 8 7 4 |
 2 4 8 | 9 5 7 | 1 3 6 |
 7 6 3 | 4 1 8 | 2 5 9 |

   | X |  Nivel: 88 (8, 8) K: 9 Tableros: i: -1 s: -1 o: 9 Origen (0, 0), K: 4, L: 0, Total 455 ms.
   |3%| [===>                                                                                                 ]   (0,6), K: 9, Nivel: 7, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 3 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 524 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 4, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 4 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 524 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 5, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 5 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 524 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 6, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 6 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 525 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 7, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 7 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 525 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 8, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 8 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 525 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 9, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 9 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 4, L: 0, Total 525 ms.

   | X |  Nivel: 0 (0, 0) K: 4 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 4, L: 0, Total 525 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 5 Tableros: i: -1 s: -1 o: 4 1631303591503
   |1%| [=>                                                                                                   ]   (0,4), K: 1, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 1 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 0 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 2, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 2 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 1 ms.
   |3%| [===>                                                                                                 ]   (0,6), K: 9, Nivel: 7, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 3 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 2 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 4, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 4 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 2 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 5, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 5 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 2 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 6, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 6 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 2 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 7, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 7 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 2 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 8, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 8 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 2 ms.
   |1%| [=>                                                                                                   ]   (0,4), K: 9, Nivel: 3, Max Nivel: 89
   | X |  Nivel: 2 (4, 0) K: 9 Tableros: i: -1 s: -1 o: 3 Origen (0, 0), K: 5, L: 0, Total 3 ms.

   | X |  Nivel: 0 (0, 0) K: 5 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 5, L: 0, Total 3 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 6 Tableros: i: -1 s: -1 o: 4 1631303591506
   |0%| [>                                                                                                    ]   (0,0), K: 6, Nivel: 1, Max Nivel: 89
   | X |  Nivel: 0 (0, 0) K: 6 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 6, L: 0, Total 0 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 7 Tableros: i: -1 s: -1 o: 4 1631303591507
   |0%| [>                                                                                                    ]   (0,0), K: 7, Nivel: 1, Max Nivel: 89
   | X |  Nivel: 0 (0, 0) K: 7 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 7, L: 0, Total 0 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 8 Tableros: i: -1 s: -1 o: 4 1631303591507
   |0%| [>                                                                                                    ]   (0,0), K: 8, Nivel: 1, Max Nivel: 89
   | X |  Nivel: 0 (0, 0) K: 8 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 8, L: 0, Total 0 ms.

.  |___|  Nivel: 0, Celda: (0, 0) K: 9 Tableros: i: -1 s: -1 o: 4 1631303591508
   |0%| [>                                                                                                    ]   (0,0), K: 9, Nivel: 1, Max Nivel: 89
   | X |  Nivel: 0 (0, 0) K: 9 Tableros: i: -1 s: -1 o: 4 Origen (0, 0), K: 9, L: 0, Total 0 ms.

Fin [SOLVING] Origen (0, 0), K: 0, L: 0, Total 647 ms.
. Tablero candidato. 
 ------ ------- -------
 4 3 5 | 2 6 9 | 7 8 1 |
 6 8 2 | 5 7 1 | 4 9 3 |
 1 9 7 | 8 3 4 | 5 6 2 |
 ------ ------- -------
 8 2 6 | 1 9 5 | 3 4 7 |
 3 7 4 | 6 8 2 | 9 1 5 |
 9 5 1 | 7 4 3 | 6 2 8 |
 ------ ------- -------
 5 1 9 | 3 2 6 | 8 7 4 |
 2 4 8 | 9 5 7 | 1 3 6 |
 7 6 3 | 4 1 8 | 2 5 9 |
Inicio [CHECK_INTEGRITY_OF_SOLUTION] 1631303591510
. Resultado Resuelto!

Fin [CHECK_INTEGRITY_OF_SOLUTION] Origen (0, 0), K: 0, L: 0, Total 1 ms.
. Tablero de comprobación... 
 ------ ------- -------
 4 3 5 | 2 6 9 | 7 8 1 |
 6 8 2 | 5 7 1 | 4 9 3 |
 1 9 7 | 8 3 4 | 5 6 2 |
 ------ ------- -------
 8 2 6 | 1 9 5 | 3 4 7 |
 3 7 4 | 6 8 2 | 9 1 5 |
 9 5 1 | 7 4 3 | 6 2 8 |
 ------ ------- -------
 5 1 9 | 3 2 6 | 8 7 4 |
 2 4 8 | 9 5 7 | 1 3 6 |
 7 6 3 | 4 1 8 | 2 5 9 |
. Tablero candidato. 
 ------ ------- -------
 4 3 5 | 2 6 9 | 7 8 1 |
 6 8 2 | 5 7 1 | 4 9 3 |
 1 9 7 | 8 3 4 | 5 6 2 |
 ------ ------- -------
 8 2 6 | 1 9 5 | 3 4 7 |
 3 7 4 | 6 8 2 | 9 1 5 |
 9 5 1 | 7 4 3 | 6 2 8 |
 ------ ------- -------
 5 1 9 | 3 2 6 | 8 7 4 |
 2 4 8 | 9 5 7 | 1 3 6 |
 7 6 3 | 4 1 8 | 2 5 9 |
Inicio [CHECK_CORRECTNESS_OF_SOLUTION] 1631303591512
. Resultado Resuelto!

Fin [CHECK_CORRECTNESS_OF_SOLUTION] Origen (0, 0), K: 0, L: 0, Total 0 ms.
Fin Corrió por -803 milliseconds.
Detenida APP Corrió por -803 milliseconds.

```

# Sudoku 2

Se ha tomado un segundo sudoku que, extrañamente (al menos para un estudiante), encuentra al menos 5 soluciones y demora al menos 92708 ms por rama con una ejecución total casi, con ironía se indica esta apreciación, convirtiéndose en un problema NP. Lo que quizás haga pensar que el diseño del algoritmo no es del todo correcto. Se agradecerían comentarios.
  
- https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/data/input_02.sudoku. 
- https://gitlab.com/ox/preda_2021_ped/-/blob/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/data/input_02.sudoku_sol. 
![](https://gitlab.com/ox/preda_2021_ped/-/raw/sudoku/codigo/ped_1_artifact/src/main/java/com/preda_ped_2021/data/input_02_sudoku.png). 


```pre
Logs en modo LOG
Arrancando APP Fri Sep 10 22:05:58 CEST 2021
Arrancando Solucionador Base 01 Fri Sep 10 22:05:58 CEST 2021
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_02.sudoku
. . expected [plain] Found [sudoku]
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_02.sudoku
. . expected [json] Found [sudoku]
Detenido Solucionador Base 01 Corrió por -30 milliseconds.
Inicio Fri Sep 10 22:05:58 CEST 2021
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_02.sudoku_sol
. Fichero leido ./src/main/java/com/preda_ped_2021/data/input_02.sudoku_sol
. Fichero cargado ./src/main/java/com/preda_ped_2021/data/input_02.sudoku_sol
. Leyendo archivo ./src/main/java/com/preda_ped_2021/data/input_02.sudoku
. Fichero leido ./src/main/java/com/preda_ped_2021/data/input_02.sudoku
. Fichero cargado ./src/main/java/com/preda_ped_2021/data/input_02.sudoku
. Tablero inicial... 
 ------ ------- -------
 . 2 . | . . . | . . . |
 . . . | 6 . . | . . 3 |
 . 7 4 | . . 8 | . . . |
 ------ ------- -------
 . . . | . . 3 | . . 2 |
 . 8 . | . 4 . | . 1 . |
 6 . . | 5 . . | . . . |
 ------ ------- -------
 . . . | . 1 . | 7 8 . |
 5 . . | . . 9 | . . . |
 . . . | . . . | . 4 . |
Inicio [SOLVING] 1631304358438

.  |___|  Nivel: 0, Celda: (0, 0) K: 1 Tableros: i: -1 s: -1 o: 1 1631304358439
   |1%| [=>                                                                                                   ]   (0,1), K: 1, Nivel: 3, Max Nivel: 3
   | X |  Nivel: 2 (1, 0) K: 1 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 9 ms.
   |3%| [===>                                                                                                 ]   (0,3), K: 9, Nivel: 7, Max Nivel: 41
   | X |  Nivel: 2 (1, 0) K: 2 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 10225 ms.
   |1%| [=>                                                                                                   ]   (0,1), K: 3, Nivel: 3, Max Nivel: 41
   | X |  Nivel: 2 (1, 0) K: 3 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 10225 ms.
   |1%| [=>                                                                                                   ]   (0,1), K: 4, Nivel: 3, Max Nivel: 41
   | X |  Nivel: 2 (1, 0) K: 4 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 10225 ms.
   |1%| [=>                                                                                                   ]   (0,1), K: 5, Nivel: 3, Max Nivel: 41
   | X |  Nivel: 2 (1, 0) K: 5 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 10225 ms.
   |1%| [=>                                                                                                   ]   (0,1), K: 6, Nivel: 3, Max Nivel: 41
   | X |  Nivel: 2 (1, 0) K: 6 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 10274 ms.
   |3%| [===>                                                                                                 ]   (0,3), K: 9, Nivel: 7, Max Nivel: 41
   | X |  Nivel: 2 (1, 0) K: 7 Tableros: i: -1 s: -1 o: 8 Origen (0, 0), K: 1, L: 0, Total 23627 ms.
  |61%| [=============================================================>                                       ] (8,8), K: 9, Nivel: 123, Max Nivel: 123
. Solución encontrada 
 ------ ------- -------
 1 2 6 | 3 5 4 | 9 7 8 |
 8 5 9 | 6 7 1 | 4 2 3 |
 3 7 4 | 2 9 8 | 1 6 5 |
 ------ ------- -------
 4 9 7 | 1 8 3 | 6 5 2 |
 2 8 5 | 9 4 6 | 3 1 7 |
 6 1 3 | 5 2 7 | 8 9 4 |
 ------ ------- -------
 9 3 2 | 4 1 5 | 7 8 6 |
 5 4 8 | 7 6 9 | 2 3 1 |
 7 6 1 | 8 3 2 | 5 4 9 |

   | X |  Nivel: 122 (8, 8) K: 9 Tableros: i: -1 s: -1 o: 9 Origen (0, 0), K: 1, L: 0, Total 54488 ms.
  |61%| [=============================================================>                                       ] (8,8), K: 9, Nivel: 123, Max Nivel: 123
. Solución encontrada 
 ------ ------- -------
 1 2 6 | 4 3 5 | 9 7 8 |
 8 5 9 | 6 7 1 | 4 2 3 |
 3 7 4 | 2 9 8 | 1 5 6 |
 ------ ------- -------
 4 9 7 | 1 8 3 | 5 6 2 |
 2 8 5 | 9 4 6 | 3 1 7 |
 6 1 3 | 5 2 7 | 8 9 4 |
 ------ ------- -------
 9 6 2 | 3 1 4 | 7 8 5 |
 5 4 8 | 7 6 9 | 2 3 1 |
 7 3 1 | 8 5 2 | 6 4 9 |

   | X |  Nivel: 122 (8, 8) K: 9 Tableros: i: -1 s: 9 o: 9 Origen (0, 0), K: 1, L: 0, Total 69010 ms.
  |61%| [=============================================================>                                       ] (8,8), K: 9, Nivel: 123, Max Nivel: 123
. Solución encontrada 
 ------ ------- -------
 1 2 6 | 4 3 5 | 9 7 8 |
 8 5 9 | 6 7 1 | 4 2 3 |
 3 7 4 | 2 9 8 | 1 5 6 |
 ------ ------- -------
 4 9 7 | 1 8 3 | 5 6 2 |
 2 8 5 | 9 4 6 | 3 1 7 |
 6 3 1 | 5 2 7 | 8 9 4 |
 ------ ------- -------
 9 6 2 | 3 1 4 | 7 8 5 |
 5 4 8 | 7 6 9 | 2 3 1 |
 7 1 3 | 8 5 2 | 6 4 9 |

   | X |  Nivel: 122 (8, 8) K: 9 Tableros: i: -1 s: 9 o: 9 Origen (0, 0), K: 1, L: 0, Total 92708 ms.
  |61%| [=============================================================>                                       ] (8,8), K: 9, Nivel: 123, Max Nivel: 123
. Solución encontrada 
 ------ ------- -------
 1 2 6 | 3 5 4 | 9 7 8 |
 8 5 9 | 6 7 1 | 4 2 3 |
 3 7 4 | 2 9 8 | 1 5 6 |
 ------ ------- -------
 4 9 7 | 1 8 3 | 5 6 2 |
 2 8 5 | 9 4 6 | 3 1 7 |
 6 3 1 | 5 2 7 | 8 9 4 |
 ------ ------- -------
 9 6 3 | 4 1 2 | 7 8 5 |
 5 4 8 | 7 6 9 | 2 3 1 |
 7 1 2 | 8 3 5 | 6 4 9 |

   | X |  Nivel: 122 (8, 8) K: 9 Tableros: i: -1 s: 9 o: 9 Origen (0, 0), K: 1, L: 0, Total 94725 ms.
  |36%| [====================================>                                                                ]  (5,1), K: 9, Nivel: 73, Max Nivel: 123
(EL LOG SIGUE)
```


**Nota tras la experiencia**: es fundamental entender bien la estructura del algortimo a usar, en este caso, vuelta atrás, antes de intentar trasladarlo al lenguaje de programación elegido.

## Histórico
Este proyecto es demo y puede ser clonado para resolver cualquier problema del histórico. Aunque pudiera ser más intituivo usar el boilerplate desde la rama "ped", se recomienda usar la "sudoku" creando un nuevo objecto que herede de Solver.

![](https://gitlab.com/ox/preda_2021_ped/-/raw/sudoku/HISTORICO.png)


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ox/preda_2021_ped.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:6339821cd446eb5dc44150a0d2ccf1c8?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

